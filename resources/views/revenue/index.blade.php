@extends('layouts.default')

<?php

/**
 * @var $revenues
 */

?>

@section('content')
<section class="content-header">
    <h1>
        Doanh thu
        <small>list</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Doanh thu</li>
    </ol>

</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">Gói</th>
                            <th class="text-center">Học viên</th>
                            <th class="text-center">Số điện thoại</th>
                            <th class="text-center">Doanh thu</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($revenues as $revenue)
                            <tr>
                                <td class="text-center">
                                    {{$revenue['id']}}
                                </td>
                                <td class="text-center">
                                    {{$revenue['package']['name']}}
                                </td>
                                <td class="text-center">
                                    {{$revenue['student']['name']}}
                                </td>
                                <td class="text-center">
                                    {{$revenue['student']['mobile']}}
                                </td>
                                <td class="text-center">
                                    {{$revenue['package']['price']}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="text-center">

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td class="text-right text-bold">
                                    Tổng
                                </td>
                                <td class="text-center text-bold">
                                    {{$total}}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
