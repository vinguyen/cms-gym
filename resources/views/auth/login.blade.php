@extends('layouts.auth')

@section('content')

    <div class="login-box-body">
        <h3 class="login-box-msg">Sign in</h3>

        <form action="{{route("login")}}" method="POST">
            @csrf
            <div class="form-group has-feedback">
                <input type="email" name="email" value="admin@gmail.com" class="form-control" placeholder="Email">
                <span class="fa fa-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" value="123456" class="form-control" placeholder="Password">
                <span class="fa fa-lock form-control-feedback"></span>
            </div>
            <div class="row" style="margin-left: 0">
                <div class="col-xs-8" style="padding-left: 20px;">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="#">I forgot my password</a><br>

    </div>

@stop
