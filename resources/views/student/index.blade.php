@extends('layouts.default')


<?php

/**
 * @var $students
 * @var $checkPermission
 */

?>

@section('content')
<section class="content-header">
    <h1>
        Student
        <small>list</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Student</li>
    </ol>

</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if($checkPermission["add"])
                        <a href="{{route('student.create')}}">
                            <button class="btn btn-flat btn-success btn-create">
                                Thêm mới
                            </button>
                        </a>
                    @endif
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Số điện thoại</th>
                            <th class="text-center">Năm sinh</th>
                            <th class="text-center">Giới tính</th>
                            <th class="text-center">Gói đang tập</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($students as $student)
                            <tr>
                                <td class="text-center">
                                    {{$student['id']}}
                                </td>
                                <td class="text-center">
                                    {{$student['name']}}
                                </td>
                                <td class="text-center">
                                    {{$student['mobile']}}
                                </td>
                                <td class="text-center">
                                    {{$student['yearOfBirth']}}
                                </td>
                                <td class="text-center">
                                    {{$student['gender']}}
                                </td>
                                <td class="text-center">
                                    {{$student['package_name']}}
                                </td>
                                <td class="text-center">
                                    @if($checkPermission['view'])
                                        <button class="btn btn-sm btn-warning">
                                            View
                                        </button>
                                    @endif
                                    @if($checkPermission['edit'])
                                        <a href="{{route('student.edit',$student['id'])}}">
                                            <button class="btn btn-sm btn-primary">
                                                Edit
                                            </button>
                                        </a>
                                    @endif
                                    @if($checkPermission['delete'])
                                        <a href="{{route('student.destroy',$student['id'])}}">
                                            <button class="btn btn-sm btn-danger">
                                                Delete
                                            </button>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@stop
