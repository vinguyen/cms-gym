@extends('layouts.default')

<?php
/**
 * @var $packages
 */
?>

@section('content')
<section class="content-header">
    <h1>
        Học viên
        <small>Tạo</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Học viên</li>
    </ol>

</section>

<section class="content">
    <div class="row">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Thêm học viên</h3>
            </div>
            <div class="box-body">
                <form action="{{route('student.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Tên</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter ...">
                    </div>

                    <div class="form-group">
                        <label>Số điện thoại</label>
                        <input type="text" name="mobile" class="form-control" placeholder="Enter ...">
                    </div>

                    <div class="form-group">
                        <label>Năm sinh</label>
                        <input type="number" name="yearOfBirth" class="form-control" placeholder="Enter ...">
                    </div>

                    <div class="form-group">
                        <label>Giới tính</label>
                        <select name="gender" class="form-control">
                            <option value="">Chọn giới tính</option>
                            <option value="Nam">Nam</option>
                            <option value="Nữ">Nữ</option>
                        </select>
                    </div>

                    <!-- select -->
                    <div class="form-group">
                        <label>Chọn gói tập</label>
                        <select name="package_id" class="form-control">
                            <option value="">Chọn gói tập</option>
                            @foreach($packages as $package)
                                <option value="{{$package['id']}}">{{$package['name']}} - ( {{$package['period']}} ngày )</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary">
                            Thêm mới
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>

@stop
