@extends('layouts.default')

<?php
/**
 * @var $student
 */
?>

@section('content')
    <section class="content-header">
        <h1>
            Học viên
            <small>Cập nhật</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Học viên</li>
        </ol>

    </section>

    <section class="content">
        <div class="row">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Cập nhật thông tin học viên</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('student.update',$student['id'])}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label>Tên</label>
                            <input type="text" value="{{$student['name']}}" name="name" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Số điện thoại</label>
                            <input type="text" value="{{$student['mobile']}}" name="mobile" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Năm sinh</label>
                            <input type="number" value="{{$student['yearOfBirth']}}" name="yearOfBirth" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Giới tính</label>
                            <select name="gender" class="form-control">
                                <option value="">Chọn giới tính</option>
                                @if($student['gender']=="Nam")
                                    <option value="Nam" selected >Nam</option>
                                @else
                                    <option value="Nam" >Nam</option>
                                @endif
                                @if($student['gender']=="Nữ")
                                    <option value="Nữ" selected>Nữ</option>
                                @else
                                    <option value="Nữ">Nữ</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">
                                Cập nhật
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
