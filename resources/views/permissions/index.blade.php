@extends('layouts.default')

<?php

    /**
     * @var $permissions
     * @var $checkPermission
    */

?>


@section('content')

    <section class="content-header">
        <h1>
            Trang quản trị
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Trang quản trị</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @if($checkPermission["add"])
                        <a href="{{route('permission.create')}}">
                            <button class="btn btn-flat btn-success btn-create">
                                Thêm mới
                            </button>
                        </a>
                        @endif
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Name</th>
                                <th class="text-center">Url</th>
                                <th class="text-center">Permission Code</th>
                                <th class="text-center">Action</th>
                                <th class="text-center">Show Menu</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($permissions as $permission)
                                <tr>
                                    <td class="text-center">
                                        {{$permission['id']}}
                                    </td>
                                    <td>
                                        {{$permission['display_name']}}
                                    </td>
                                    <td class="text-center">
                                        {{$permission['url']}}
                                    </td>
                                    <td class="text-center">
                                        {{$permission['name']}}
                                    </td>
                                    <td class="text-center">
                                        <label>
                                            <input
                                                type="checkbox"
                                                value="1"
                                                @if($permission['is_display']) checked @endif
                                            >
                                        </label>
                                    </td>

                                    <td class="text-center">
                                        @if($checkPermission['edit'])
                                        <a href="{{route('permission.edit',$permission['id'])}}">
                                            <button class="btn btn-sm btn-warning">
                                                Edit
                                            </button>
                                        </a>
                                        @endif
                                        @if($checkPermission['delete'])
                                            <a href="{{route('permission.destroy',$permission['id'])}}">
                                                <button class="btn btn-sm btn-danger">
                                                    Delete
                                                </button>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                @if(count($permission['children']))
                                    @include('components.permission_child',[
                                        'permissions'=>$permission['children'],
                                        'tree'=>'-------',
                                        'checkPermission'=>$checkPermission
                                    ])
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        .sub-hr {
            width: 18px;
            display: inline-block;
        }
    </style>

@stop
