@extends('layouts.default')

<?php

/**
 * @var $permissions
 * @var $role_id
 * @var $role_name
 */

?>

@section('content')

    <section class="content-header">
        <h1>
            Permission
            <small>Role</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Permission</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Permission for {{$role_name}}</h3>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Name</th>
                                <th class="text-center">Browser</th>
                                <th class="text-center">View</th>
                                <th class="text-center">Edit</th>
                                <th class="text-center">Delete</th>
                                <th class="text-center">Add</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($permissions as $permission)
                                <tr>
                                    <td class="text-center">
                                        {{$permission['id']}}
                                    </td>
                                    <td>
                                        {{$permission['display_name']}}
                                    </td>
                                    <td class="text-center">
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input
                                                            type="checkbox"
                                                            value="1"
                                                            @if($permission['checked']) checked @endif
                                                            onclick="checkPermission({{$permission['id']}})"
                                                        >
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    @foreach($permission['children_model'] as $child)
                                        <td class="text-center">
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input
                                                                type="checkbox"
                                                                value="1"
                                                                @if($child['checked']) checked @endif
                                                                onclick="checkPermission({{$child['id']}})"
                                                            >
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    @endforeach
                                </tr>
                                @if(count($permission['children']))
                                    @include('components.assign_permission_child',[
                                        'permissions'=>$permission['children'],
                                        'tree'=>'-------'
                                    ])
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        function checkPermission(permission_id) {
            $.ajax({
                url: '/permission/role',
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    permission_id: permission_id,
                    role_id: {{$role_id}}
                },
                success: function (response) {

                    console.log(response);
                },
            });
        }
    </script>

@stop
