@extends('layouts.default')

<?php
/**
 * @var $role
 * @var $permission
 */
?>

@section('content')
    <section class="content-header">
        <h1>
            Permission
            <small>Update</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Permission</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$permission['display_name']}}</h3>
                    </div>
                    <div class="box-body">
                        <form action="{{route('permission.update',$permission['id'])}}" method="post">
                        @csrf
                        <!-- select -->
                            <div class="form-group">
                                <label for="parent_id" >Thư mục cha</label>
                                <select id="parent_id" name="parent_id" class="form-control">
                                    <option value="">Chọn thư mục cha</option>
                                    @foreach($menus as $menu)
                                        @if($menu['id']==$permission['parent_id'])
                                            <option value="{{$menu['id']}}" selected>{{$menu['display_name']}}</option>
                                        @endif
                                        <option value="{{$menu['id']}}" >{{$menu['display_name']}}</option>
                                        @if(count($menu['children']))
                                            @include('components.option_child',[
                                                'children'=>$menu['children'],
                                                'tree'=>"-----"
                                            ])
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Tên trang</label>
                                <input type="text"
                                       name="name"
                                       class="form-control"
                                       placeholder="Enter ..."
                                       value="{{$permission['display_name']}}"
                                >
                            </div>

                            <div class="form-group">
                                <label>Url</label>
                                <input type="text"
                                       name="url"
                                       class="form-control"
                                       placeholder="Enter ..."
                                       value="{{$permission['url']}}"
                                >
                            </div>

                            <div class="form-group">
                                <label>Icon</label>
                                <input type="text"
                                       name="icon"
                                       class="form-control"
                                       placeholder="Enter ..."
                                       value="{{$permission['icon']}}"
                                >
                            </div>

                            <div class="form-group">
                                <label>
                                    <input
                                        type="checkbox"
                                        name="is_display"
                                        {{$permission['is_display']?'checked':""}}
                                    > Show in menu
                                </label>
                            </div>

                            <!-- textarea -->
                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea name="description"
                                          class="form-control"
                                          rows="3" placeholder="Enter ..."
                                          value="{{$permission['description']}}"
                                ></textarea>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">
                                    Cập nhật
                                </button>
                            </div>

                        </form>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Role Name</th>
                                <th class="text-center">Browser</th>
                                <th class="text-center">View</th>
                                <th class="text-center">Edit</th>
                                <th class="text-center">Delete</th>
                                <th class="text-center">Add</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td class="text-center">
                                        {{$role['id']}}
                                    </td>
                                    <td class="text-center">
                                        {{$role['display_name']}}
                                    </td>
                                    @foreach($permissionNames as $permissionName)
                                        <td class="text-center">
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input
                                                                type="checkbox"
                                                                value="1"
                                                                @if($role[$permissionName['name']]) checked @endif
                                                                onclick="checkPermission({{$permissionName['id']}},{{$role['id']}})"
                                                            >
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        function checkPermission(permission_id,role_id) {
            $.ajax({
                url: '/permission/role',
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    permission_id: permission_id,
                    role_id: role_id
                },
                success: function (response) {

                    console.log(response);
                },
            });
        }
    </script>
@stop
