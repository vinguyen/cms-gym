@extends('layouts.default')

@section('content')

<section class="content-header">
    <h1>
        Phân quyền
        <small>Trang quản trị</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Phân quyền</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Thêm mới</h3>
            </div>
            <div class="box-body">
                <form method="post">
                @csrf
                    <!-- select -->
                    <div class="form-group">
                        <label for="parent_id" >Thư mục cha</label>
                        <select id="parent_id" name="parent_id" class="form-control">
                            <option value="">Chọn thư mục cha</option>
                            @foreach($menus as $menu)
                                <option value="{{$menu['id']}}" >{{$menu['display_name']}}</option>
                                @if(count($menu['children']))
                                    @include('components.option_child',[
                                        'children'=>$menu['children'],
                                        'tree'=>"-----"
                                    ])
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Tên trang</label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Enter ...">
                    </div>

                    <div class="form-group">
                        <label>Url</label>
                        <input type="text" id="url" name="url" class="form-control" placeholder="Enter ...">
                    </div>

                    <div class="form-group">
                        <label>Icon</label>
                        <input type="text" id="icon" name="icon" class="form-control" placeholder="Enter ...">
                    </div>

                    <div class="form-group">
                        <label>
                            <input
                                type="checkbox"
                                name="is_display"
                                id="is_display"
                            > Show in menu
                        </label>
                    </div>

                    <!-- textarea -->
                    <div class="form-group">
                        <label>Mô tả</label>
                        <textarea id="description" name="description" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                    </div>
                </form>
                <div class="form-group">
                    <button
                        class="btn btn-primary"
                        onclick="onSubmit()"
                    >
                        Thêm mới
                    </button>
                    <button
                        class="btn btn-info"
                        onclick="onPermission()"
                    >
                        Phân quyền
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function onSubmit() {
        let formData = {
            "_token": "{{ csrf_token() }}",
            name: $("#name").val(),
            parent_id: $("#parent_id").val(),
            url: $("#url").val(),
            icon: $("#icon").val(),
            is_display: $("#is_display").val(),
            description: $("#description").val(),
        }

        $.ajax({
            url: "/permission",
            type: "POST",
            data: formData,
            success: function (res) {
                location.href = "/permission"
            }
        })
    }

    function onPermission() {
        let formData = {
            "_token": "{{ csrf_token() }}",
            name: $("#name").val(),
            parent_id: $("#parent_id").val(),
            url: $("#url").val(),
            icon: $("#icon").val(),
            is_display: $("#is_display").val(),
            description: $("#description").val(),
            permission: true
        }

        $.ajax({
            url: "/permission",
            type: "POST",
            data: formData,
            success: function (res) {
                location.href = "/permission/"+res.permission_id+"/edit"
            }
        })
    }

</script>

@stop
