<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/ionicons.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/AdminLTE.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/_all-skins.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

