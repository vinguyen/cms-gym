<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="https://www.w3schools.com/w3images/avatar2.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ $user_name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <li class="active">
                <a href="{{route('dashboard')}}">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            @if($nav_menus)
                @foreach($nav_menus as $menu)
                <li class="treeview {{$menu['is_active']?"active":""}}">
                    <a>
                        <i class="{{$menu['icon']}}"></i>
                        <span>{{$menu['display_name']}}</span>
{{--                        <span onclick="viewMenu('{{$menu['url']}}')">{{$menu['display_name']}}</span>--}}
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    @if(count($menu['menu_children_user']))
                        @include('components.menu_child',['children'=>$menu['menu_children_user']])
                    @endif
                </li>
                @endforeach
            @endif

        </ul>
    </section>
</aside>

<script>
    function viewMenu(url) {
        if (url) {
            location.href = url;
        }
    }
</script>
