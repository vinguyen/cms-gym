@extends('layouts.default')

@section('content')

    <section class="content-header">
        <h1>
            Thiết bị
            <small>Thêm mới</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Thiết bị</li>
        </ol>

    </section>
    <section class="content">
        <div class="row">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Thêm thiết bị</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('device.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label>Tên</label>
                            <input type="text" name="name" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Số lượng</label>
                            <input type="number" name="amount" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Mô tả</label>
                            <textarea name="description" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">
                                Thêm mới
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
