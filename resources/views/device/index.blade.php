@extends('layouts.default')

<?php

/**
 * @var $devices
 * @var $checkPermission
 */

?>

@section('content')
<section class="content-header">
    <h1>
        Thiết bị
        <small>danh sách</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Thiết bị</li>
    </ol>

</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if($checkPermission["add"])
                        <a href="{{route('device.create')}}">
                            <button class="btn btn-flat btn-success btn-create">
                                Thêm mới
                            </button>
                        </a>
                    @endif
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Created Time</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($devices as $device)
                            <tr>
                                <td class="text-center">
                                    {{$device['id']}}
                                </td>
                                <td class="text-center">
                                    {{$device['name']}}
                                </td>
                                <td class="text-center">
                                    {{$device['amount']}}
                                </td>
                                <td class="text-center">
                                    {{$device['created_time']}}
                                </td>
                                <td class="text-center">
                                    @if($checkPermission['view'])
                                        <button class="btn btn-sm btn-warning">
                                            View
                                        </button>
                                    @endif
                                    @if($checkPermission['edit'])
                                        <a href="{{route('device.edit',$device['id'])}}">
                                            <button class="btn btn-sm btn-primary">
                                                Edit
                                            </button>
                                        </a>
                                    @endif
                                    @if($checkPermission['delete'])
                                        <a href="{{route('device.destroy',$device['id'])}}">
                                            <button class="btn btn-sm btn-danger">
                                                Delete
                                            </button>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@stop
