@extends('layouts.default')

<?php

    /**
     * @var $device
    */

?>

@section('content')

    <section class="content-header">
        <h1>
            Thiết bị
            <small>create</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Thiết bị</li>
        </ol>

    </section>
    <section class="content">
        <div class="row">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Cập nhật thông tin thiết bị</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('device.update',$device['id'])}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label>Tên</label>
                            <input type="text" name="name" value="{{$device['name']}}" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Số lượng</label>
                            <input type="number" name="amount" value="{{$device['amount']}}" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Mô tả</label>
                            <textarea
                                name="description"
                                class="form-control"
                                rows="3"
                                placeholder="Enter ...">{{$device['description']}}</textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">
                                Cập nhật
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
