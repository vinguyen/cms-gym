<!doctype html>
<html lang="en">
<head>
    @include('elements.header.meta',[])
    @include('elements.header.style',[])

</head>
<body class="hold-transition login-page">

<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>CMS GYM</b></a>
    </div>

    @yield('content')

</div>

@include('elements.header.script',[])
</body>
</html>
