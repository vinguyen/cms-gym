<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('elements.header.meta',[])
    @include('elements.header.style',[])

</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
    @include('elements.navbar.header')

    @include('elements.navbar.nav')

    <div class="content-wrapper">

        @yield('content')

    </div>

    @include('elements.navbar.footer')
</div>

@include('elements.header.script',[])
</body>
</html>
