@extends('layouts.default')

<?php

    /**
     * @var $users
     * @var $checkPermission
    */

?>

@section('content')

<section class="content-header">
    <h1>
        User
        <small>list</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User</li>
    </ol>

</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if($checkPermission["add"])
                    <a href="{{route('user.create')}}">
                        <button class="btn btn-flat btn-success btn-create">
                            Thêm mới
                        </button>
                    </a>
                    @endif
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Role Name</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td class="text-center">
                                    {{$user['user_id']}}
                                </td>
                                <td class="text-center">
                                    {{$user['name']}}
                                </td>
                                <td class="text-center">
                                    {{$user['email']}}
                                </td>
                                <td class="text-center">
                                    {{$user['role_display_name']}}
                                </td>
                                <td class="text-center">
                                    @if($checkPermission['view'])
                                    <button class="btn btn-sm btn-warning">
                                        View
                                    </button>
                                    @endif
                                    @if($checkPermission['edit'])
                                    <a href="{{route('user.edit',$user['user_id'])}}">
                                        <button class="btn btn-sm btn-primary">
                                            Edit
                                        </button>
                                    </a>
                                    @endif
                                    @if($checkPermission['delete'])
                                    <a href="{{route('user.destroy',$user['user_id'])}}">
                                        <button class="btn btn-sm btn-danger">
                                            Delete
                                        </button>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


@stop
