@extends('layouts.default')

<?php
/**
 * @var $roles
 * @var $user
 */
?>

@section('content')

    <section class="content-header">
        <h1>
            User
            <small>create</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User</li>
        </ol>

    </section>
    <section class="content">
        <div class="row">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Cập nhật thông tin user</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('user.update',$user['user_id'])}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label>Tên</label>
                            <input type="text" value="{{$user['name']}}" name="name" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" value="{{$user['email']}}" name="email" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Enter ...">
                        </div>

                        <!-- select -->
                        <div class="form-group">
                            <label>Vai trò</label>
                            <select name="role_name" class="form-control">
                                <option value="">Chọn vai trò</option>
                                @foreach($roles as $role)
                                    @if($role['name']==$user['role_name'])
                                        <option value="{{$role['name']}}" selected>{{$role['display_name']}}</option>
                                    @else
                                        <option value="{{$role['name']}}">{{$role['display_name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">
                                Cập nhật
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
