<ul class="treeview-menu">
    @foreach($children as $child)
        <li class="treeview {{$child['is_active']?"active":""}}">
            <a>
                <i class="fa fa-circle-o"></i>
                <span onclick="viewMenu('{{$child['url']}}')">
                    {{$child['display_name']}}
                </span>
                @if(count($child['children']))
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                @endif
            </a>
            @if($child['menu_children_user'])
                @include('components.menu_child',['children'=>$child['menu_children_user']])
            @endif
        </li>
    @endforeach
</ul>

<style>
    .treeview-menu {
        padding-left: 20px!important;
    }
</style>
