@foreach($permissions as $permission)
    <tr>
        <td class="text-center">
            {{$permission['id']}}
        </td>
        <td>
            {{$tree}}
            {{$permission['display_name']}}
        </td>
        <td class="text-center">
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input
                                type="checkbox"
                                value="1"
                                @if($permission['checked']) checked @endif
                                onclick="checkPermission({{$permission['id']}})"
                            >
                        </label>
                    </div>
                </div>
            </div>
        </td>
        @foreach($permission['children_model'] as $child)
            <td class="text-center">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input
                                    type="checkbox"
                                    value="1"
                                    @if($child['checked']) checked @endif
                                    onclick="checkPermission({{$child['id']}})"                                                             onclick="checkPermission({{$child['id']}})"--}}
                                >
                            </label>
                        </div>
                    </div>
                </div>
            </td>
        @endforeach
    </tr>
    @if(count($permission['children']))
        @include('components.assign_permission_child',[
            'permissions'=>$permission['children'],
            'tree'=>'-------'.$tree
        ])
    @endif
@endforeach
