@foreach($children as $child)
    <option value="{{$child['id']}}">
        {{$tree}}{{$child['display_name']}}
    </option>
    @if(count($child['children']))
        @include('components.option_child',[
            'children'=>$child['children'],
            'tree'=>"-----".$tree
        ])
    @endif
@endforeach
