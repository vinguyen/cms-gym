@foreach($permissions as $permission)
    <tr>
        <td class="text-center">
            {{$permission['id']}}
        </td>
        <td>
            {{$tree}}
            {{$permission['display_name']}}
        </td>
        <td class="text-center">
            {{$permission['url']}}
        </td>
        <td class="text-center">
            {{$permission['name']}}
        </td>
        <td class="text-center">
            <label>
                <input
                    type="checkbox"
                    value="1"
                    @if($permission['is_display']) checked @endif
                >
            </label>
        </td>
        <td class="text-center">
            @if($checkPermission['edit'])
                <a href="{{route('permission.edit',$permission['id'])}}">
                    <button class="btn btn-sm btn-warning">
                        Edit
                    </button>
                </a>
            @endif
            @if($checkPermission['delete'])
                <a href="{{route('permission.destroy',$permission['id'])}}">
                    <button class="btn btn-sm btn-danger">
                        Delete
                    </button>
                </a>
            @endif
        </td>
    </tr>
    @if(count($permission['children']))
        @include('components.permission_child',[
            'permissions'=>$permission['children'],
            'tree'=>$tree."-------",
            'checkPermission'=>$checkPermission
        ])
    @endif
@endforeach
