@extends('layouts.default')

<?php
/**
 * @var $menu
 * @var $menu_parent
 */
?>

@section('content')
    <section class="content-header">
        <h1>
            Menu
            <small>Create</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Menu</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Cập nhật menu</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('menu_cms.update',$menu['id'])}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <!-- text input -->
                        <div class="form-group">
                            <label>Tên menu</label>
                            <input type="text" value="{{$menu['name']}}" name="name" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Url</label>
                            <input type="text" value="{{$menu['url']}}" name="url" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Icon</label>
                            <input type="text" value="{{$menu['icon']}}" name="icon" class="form-control" placeholder="Enter ...">
                        </div>

                        <!-- textarea -->
                        <div class="form-group">
                            <label>Mô tả</label>
                            <textarea name="description" value="{{$menu['description']}}" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                        </div>


                        <!-- select -->
                        <div class="form-group">
                            <label>Thể loại cha</label>
                            <select name="parent_id" class="form-control">

                                <option value="">Thể loại cha</option>
                                @foreach($menu_parent as $item)
                                    @if($item['id']==$menu['parent_id'])
                                        <option value="{{$item['id']}}" selected>{{$item['name']}}</option>
                                    @else
                                        <option value="{{$item['id']}}">{{$item['name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">
                                Cập nhật
                            </button>
                        </div>

                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </section>

@stop
