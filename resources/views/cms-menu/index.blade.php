@extends('layouts.default')

<?php

    /**
     * @var $menus
    */

?>

@section('content')

    <section class="content-header">
        <h1>
            Cms menu
            <small>list</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">CMS Menu</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="{{route('menu_cms.create')}}">
                            <button class="btn btn-flat btn-success btn-create">
                                Thêm mới
                            </button>
                        </a>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th>Name</th>
                                    <th class="text-center">Category Parent</th>
{{--                                    <th class="text-center">Order</th>--}}
{{--                                    <th class="text-center">Status</th>--}}
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($menus as $menu)
                                <tr>
                                    <td class="text-center">{{$menu['id']}}</td>
                                    <td>
                                        {{$menu['name']}}
                                    </td>
                                    <td class="text-center"></td>
{{--                                    <td class="text-center">1</td>--}}
{{--                                    <td class="text-center">1</td>--}}
                                    <td class="text-center">
                                        <a href="{{route("menu_cms.edit",$menu['id'])}}">
                                            <button class="btn btn-sm btn-warning">
                                                Edit
                                            </button>
                                        </a>
                                        <a href="{{route("menu_cms.destroy",$menu['id'])}}">
                                            <button class="btn btn-sm btn-danger">
                                                Delete
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                                @foreach($menu['children'] as $children)
                                    <tr>
                                        <td class="text-center">{{$menu['id']}}</td>
                                        <td>
                                            <span class="sub-icon"></span>
                                            {{$children['name']}}
                                        </td>
                                        <td class="text-center">{{$menu['name']}}</td>
{{--                                        <td class="text-center">1</td>--}}
{{--                                        <td class="text-center">1</td>--}}
                                        <td class="text-center">
                                            <a href="{{route("menu_cms.edit",$children['id'])}}">
                                                <button class="btn btn-sm btn-warning">
                                                    Edit
                                                </button>
                                            </a>
                                            <a href="{{route("menu_cms.destroy",$children['id'])}}">
                                                <button class="btn btn-sm btn-danger">
                                                    Delete
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </section>


@stop
