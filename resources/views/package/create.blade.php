@extends('layouts.default')

@section('content')
<section class="content-header">
    <h1>
        Gói tập
        <small>Thêm mới</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gói tập</li>
    </ol>

</section>

<section class="content">
    <div class="row">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title">Thêm gói tập mới</h3>
            </div>
            <div class="box-body">
                <div class="box-body">
                    <form action="{{route('package.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label>Tên</label>
                            <input type="text" name="name" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Giá (VNĐ)</label>
                            <input type="number" name="price" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Số ngày tập (ngày)</label>
                            <input type="number" name="period" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Mô tả</label>
                            <textarea name="description" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">
                                Thêm mới
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@stop
