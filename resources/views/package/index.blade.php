@extends('layouts.default')

<?php

/**
 * @var $packages
 * @var $checkPermission
 */

?>

@section('content')
<section class="content-header">
    <h1>
        Gói tập
        <small>Danh sách</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gói tập</li>
    </ol>

</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if($checkPermission["add"])
                        <a href="{{route('package.create')}}">
                            <button class="btn btn-flat btn-success btn-create">
                                Thêm mới
                            </button>
                        </a>
                    @endif
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">Tên</th>
                            <th class="text-center">Giá (VNĐ)</th>
                            <th class="text-center">Số ngày</th>
                            <th class="text-center">Hành động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($packages as $package)
                            <tr>
                                <td class="text-center">
                                    {{$package['id']}}
                                </td>
                                <td class="text-center">
                                    {{$package['name']}}
                                </td>
                                <td class="text-center">
                                    {{$package['price']}}
                                </td>
                                <td class="text-center">
                                    {{$package['period']}}
                                </td>
                                <td class="text-center">
                                    @if($checkPermission['view'])
                                        <button class="btn btn-sm btn-warning">
                                            View
                                        </button>
                                    @endif
                                    @if($checkPermission['edit'])
                                        <a href="{{route('package.edit',$package['id'])}}">
                                            <button class="btn btn-sm btn-primary">
                                                Edit
                                            </button>
                                        </a>
                                    @endif
                                    @if($checkPermission['delete'])
                                        <a href="{{route('package.destroy',$package['id'])}}">
                                            <button class="btn btn-sm btn-danger">
                                                Delete
                                            </button>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@stop
