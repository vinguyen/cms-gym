@extends('layouts.default')

<?php

/**
 * @var $package
 */

?>

@section('content')
    <section class="content-header">
        <h1>
            Gói tập
            <small>Cập nhật</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Gói tập</li>
        </ol>

    </section>

    <section class="content">
        <div class="row">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Cập nhật gói tập</h3>
                </div>
                <div class="box-body">
                    <div class="box-body">
                        <form action="{{route('package.update',$package['id'])}}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <label>Tên</label>
                                <input type="text" value="{{$package['name']}}" name="name" class="form-control" placeholder="Enter ...">
                            </div>

                            <div class="form-group">
                                <label>Giá (VNĐ)</label>
                                <input type="number" value="{{$package['price']}}" name="price" class="form-control" placeholder="Enter ...">
                            </div>

                            <div class="form-group">
                                <label>Số ngày tập (ngày)</label>
                                <input type="number" value="{{$package['period']}}" name="period" class="form-control" placeholder="Enter ...">
                            </div>

                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea name="description" class="form-control" rows="3" placeholder="Enter ...">{{$package['description']}}</textarea>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">
                                    Cập nhật
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
