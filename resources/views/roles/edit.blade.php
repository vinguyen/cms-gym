@extends('layouts.default')

<?php
/**
 * @var $role
*/
?>

@section('content')

    <section class="content-header">
        <h1>
            Role
            <small>Update</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Role</li>
        </ol>

    </section>

    <section class="content">
        <div class="row">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Cập nhật vai trò</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('role.update',$role['id'])}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label>Tên vai trò (*)</label>
                            <input type="text" name="name" value="{{$role['display_name']}}" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Mã vai trò (*)</label>
                            <input type="text" name="guard_name" value="{{$role['name']}}" class="form-control" placeholder="Enter ...">
                        </div>

                        <div class="form-group">
                            <label>Mô tả</label>
                            <textarea name="description" class="form-control" value="{{$role['description']}}" rows="3" placeholder="Enter ..."></textarea>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary">
                                Cập nhật
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
