@extends('layouts.default')

<?php

    /**
     * @var $roles
     * @var $checkPermission
    */

?>

@section('content')

<section class="content-header">
    <h1>
        Role
        <small>list</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Role</li>
    </ol>

</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if($checkPermission["add"])
                    <a href="{{route('role.create')}}">
                        <button class="btn btn-flat btn-success btn-create">
                            Thêm mới
                        </button>
                    </a>
                    @endif
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <td class="text-center">
                                    {{$role['id']}}
                                </td>
                                <td class="text-center">
                                    {{$role['display_name']}}
                                </td>
                                <td class="text-center">
                                    {{$role['status_label']}}
                                </td>
                                <td class="text-center">
                                    <a href="{{route('permission.showViewRole',$role['id'])}}">
                                        <button class="btn btn-sm btn-info">
                                            Permission
                                        </button>
                                    </a>
                                    @if($checkPermission['view'])
                                    <button class="btn btn-sm btn-primary">
                                        View
                                    </button>
                                    @endif
                                    @if($checkPermission['edit'])
                                    <a href="{{route('role.edit',$role['id'])}}">
                                        <button class="btn btn-sm btn-warning">
                                            Edit
                                        </button>
                                    </a>
                                    @endif
                                    @if($checkPermission['delete'])
                                    <a href="{{route('role.destroy',$role['id'])}}">
                                        <button class="btn btn-sm btn-danger">
                                            Delete
                                        </button>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@stop
