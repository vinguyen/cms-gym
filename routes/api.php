<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
], function () {
    Route::post('login','AuthController@login');
    Route::post('register','AuthController@register');
    Route::post('logout', 'AuthController@logout');
});

Route::group(['middleware' => 'jwt.auth'], function () {

});
