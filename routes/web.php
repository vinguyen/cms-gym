<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'auth'], function () {
    Route::get('/login', 'Cms\AuthController@showLoginForm')->name("showLoginForm");
    Route::post('/login', 'Cms\AuthController@login')->name("login");
    Route::get('/logout','Cms\AuthController@logout')->name('logout');
});

Route::group(['middleware'=>['auth','route_permission']], function () {
    Route::get('/', function () {
        return view('welcome');
    })->withoutMiddleware('route_permission')->name("dashboard");

    Route::get('/unauthorized', function () {
        return view("layouts.unauthorized");
    })->withoutMiddleware('route_permission')->name('unauthorized');

    Route::group(['prefix'=>'menu_cms'], function () {
        Route::get('/', "Cms\CmsMenuController@index")->name('menu_cms.index');
        Route::get('create', "Cms\CmsMenuController@create")->name('menu_cms.create');
        Route::post('/', "Cms\CmsMenuController@store")->name('menu_cms.store');
        Route::get('/{menu_id}/edit',"Cms\CmsMenuController@edit")->name('menu_cms.edit');
        Route::put('/{menu_id}', "Cms\CmsMenuController@update")->name('menu_cms.update');
        Route::get('/{menu_id}/delete', "Cms\CmsMenuController@destroy")->name('menu_cms.destroy');
    });

    Route::group(['prefix'=>'role'], function () {
        Route::get('/', "Cms\RoleController@index")->name('role.index');
        Route::get('/create', "Cms\RoleController@create")->name('role.create');
        Route::post('/', "Cms\RoleController@store")->name('role.store');
        Route::get('/{role_id}/edit',"Cms\RoleController@edit")->name('role.edit');
        Route::put('/{role_id}', "Cms\RoleController@update")->name('role.update');
        Route::get('/{role_id}/delete', "Cms\RoleController@destroy")->name('role.destroy');
    });

    Route::group(['prefix'=>'permission'], function () {
        Route::get('/', "Cms\PermissionController@index")->name('permission.index');
        Route::get('role/{role_id}', "Cms\PermissionController@showViewRole")->name('permission.showViewRole');
        Route::post('role', "Cms\PermissionController@role")->name('permission.role');
//        Route::get('user/{user_id}', "Cms\PermissionController@showViewUser")->name('permission.showViewUser');
//        Route::post('user', "Cms\PermissionController@user")->name('permission.user');
        Route::get('/{permission_id}/edit', "Cms\PermissionController@edit")->name('permission.edit');
        Route::post('/{permission_id}', "Cms\PermissionController@update")->name('permission.update');
        Route::get('/create', "Cms\PermissionController@create")->name('permission.create');
        Route::post('/','Cms\PermissionController@store')->name('permission.store');
        Route::get('/{permission_id}/delete', "Cms\PermissionController@destroy")->name('permission.destroy');
    });

    Route::group(['prefix'=>'user'], function () {
        Route::get('/', "Cms\UserController@index")->name('user.index');
        Route::get('/create', "Cms\UserController@create")->name('user.create');
        Route::post('/', "Cms\UserController@store")->name('user.store');
        Route::get('/{user_id}/edit',"Cms\UserController@edit")->name('user.edit');
        Route::put('/{user_id}',"Cms\UserController@update")->name('user.update');
        Route::get('/{user_id}/delete',"Cms\UserController@destroy")->name('user.destroy');
    });

    Route::group(['prefix'=>'student'], function () {
        Route::get('/',"Cms\StudentController@index")->name('student.index');
        Route::get('/create', "Cms\StudentController@create")->name('student.create');
        Route::post('/', "Cms\StudentController@store")->name('student.store');
        Route::get('/{student_id}/edit',"Cms\StudentController@edit")->name('student.edit');
        Route::put('/{student_id}',"Cms\StudentController@update")->name('student.update');
        Route::get('/{student_id}/delete',"Cms\StudentController@destroy")->name('student.destroy');
    });

    Route::group(['prefix'=>'device'], function () {
        Route::get('/',"Cms\DeviceController@index")->name('device.index');
        Route::get('/create', "Cms\DeviceController@create")->name('device.create');
        Route::post('/', "Cms\DeviceController@store")->name('device.store');
        Route::get('/{device_id}/edit',"Cms\DeviceController@edit")->name('device.edit');
        Route::put('/{device_id}',"Cms\DeviceController@update")->name('device.update');
        Route::get('/{device_id}/delete',"Cms\DeviceController@destroy")->name('device.destroy');
    });

    Route::group(['prefix'=>'package'], function () {
        Route::get('/',"Cms\PackageController@index")->name('package.index');
        Route::get('/create', "Cms\PackageController@create")->name('package.create');
        Route::post('/', "Cms\PackageController@store")->name('package.store');
        Route::get('/{package_id}/edit',"Cms\PackageController@edit")->name('package.edit');
        Route::put('/{package_id}',"Cms\PackageController@update")->name('package.update');
        Route::get('/{package_id}/delete',"Cms\PackageController@destroy")->name('package.destroy');
    });

    Route::group(['prefix'=>'revenue'],function () {
        Route::get('/',"Cms\RevenueController@index")->name('revenue.index');
    });

});
