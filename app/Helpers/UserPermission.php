<?php

namespace App\Helpers;

use App\Models\User;

class UserPermission {

    public static function getAllPermissionByUser()
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        return $user->getAllPermissions()
            ->pluck('name')
            ->toArray();
    }

}
