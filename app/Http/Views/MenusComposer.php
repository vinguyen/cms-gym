<?php

namespace App\Http\Views;

use App\Models\User;
use App\Repositories\PermissionRepository;
use Illuminate\View\View;

class MenusComposer {

    protected $menus;
    protected $user_name;

    public function __construct()
    {

        $this->menus = app(PermissionRepository::class)->getMenuPermissionUser();
        /**
         * @var User $user
        */
        $user = auth()->user();
        $this->user_name = $user->name;
    }

    public function compose(View $view)
    {
        $view->with('nav_menus', $this->menus);
        $view->with('user_name', $this->user_name);
    }

}
