<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\Role;
use App\Models\User;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Carbon\Carbon as Carbon;

class UserController extends Controller
{
    /** @var UserRepository $repository */
    protected $repository;

    /**
     * Permission constructor.
     */
    public function __construct()
    {
        $this->repository = app(UserRepository::class);
    }

    public function index()
    {

        $users = $this->repository->list();
        $checkPermission = app(PermissionRepository::class)->getPermissionByRouteModel();

        $data = [
            'users'=>$users,
            'checkPermission'=>$checkPermission
        ];

        return view('user.index',$data);
    }

    public function create()
    {

        $roleRepository = app(RoleRepository::class);

        $roles = $roleRepository->list();

        $data = [
            'roles' => $roles
        ];

        return view('user.create',$data);
    }

    public function store(UserRequest $request)
    {

        $password = $request->get('password', 123456);

        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'password' => md5($password),
            'status'=> 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        $role_name = $request->get('role_name');

        /**
         * @var User $user
        */
        $user = $this->repository->create($data);

        $user->assignRole($role_name);

        return redirect()->route('user.index');
    }

    public function edit($user_id)
    {

        $user = $this->repository->findById($user_id)->toArray();

        $roleRepository = app(RoleRepository::class);

        $roles = $roleRepository->list();

        $data = [
            'user'=>$user,
            'roles'=>$roles
        ];

        return view('user.edit',$data);

    }

    public function update(UserRequest $request,$user_id)
    {
        $data = [
            'name'=>$request->get('name'),
            'email'=>$request->get('email')
        ];

        $password = $request->get('password');
        if (!empty($password)) {
            $data['password'] = md5($password);
        }

        $role_name = $request->get('role_name');

        /**
         * @var User $user
        */
        $user = $this->repository->updateOrCreate(['user_id' => $user_id], $data);
        if ($user->role_name) {
            $user->removeRole($user->role_name);
        }
        $user->assignRole($role_name);

        return redirect()->route('user.index');

    }

    public function destroy($user_id)
    {

        try {
            $this->repository->forceDelete($user_id);
        } catch (\Exception $e) {
        }

        return redirect()->route('user.index');

    }

}
