<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Repositories\DeviceRepository;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;

class DeviceController extends Controller
{

    /** @var DeviceRepository $repository */
    protected $repository;

    /**
     * Permission constructor.
     */
    public function __construct()
    {
        $this->repository = app(DeviceRepository::class);
    }

    public function index()
    {
        $devices = $this->repository->list();
        $checkPermission = app(PermissionRepository::class)->getPermissionByRouteModel();

        $data = [
            'devices' =>$devices,
            'checkPermission'=>$checkPermission
        ];

        return view('device.index',$data);
    }

    public function create()
    {
        return view('device.create');
    }

    public function store(Request $request)
    {
        $data = [
            'name'=>$request->get('name'),
            'amount'=>$request->get('amount',1),
            'description'=>$request->get('description')
        ];

        $this->repository->create($data);

        return redirect()->route('device.index');
    }

    public function edit($device_id)
    {
        $device = $this->repository->findById($device_id)->toArray();

        $data = [
            'device'=>$device
        ];

        return view("device.edit",$data);
    }

    public function update(Request $request,$device_id)
    {
        $data = [
            'name'=>$request->get('name'),
            'amount'=>$request->get('amount',1),
            'description'=>$request->get('description')
        ];

        $this->repository->updateOrCreate(['id'=>$device_id],$data);

        return redirect()->route('device.index');
    }

    public function destroy($device_id)
    {

        try {
            $this->repository->forceDelete($device_id);
        } catch (\Exception $e) {
        }

        return redirect()->route('device.index');

    }
}
