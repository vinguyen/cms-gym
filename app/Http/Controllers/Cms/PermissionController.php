<?php

namespace App\Http\Controllers\Cms;

use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PermissionController extends Controller
{
    /** @var PermissionRepository $repository */
    protected $repository;

    /**
     * Permission constructor.
     */
    public function __construct()
    {
        $this->repository = app(PermissionRepository::class);
    }

    public function index()
    {
        $permissions = $this->repository->getMenuCms();
        $checkPermission = app(PermissionRepository::class)->getPermissionByRouteModel();

        $data = [
            'permissions'=>$permissions,
            'checkPermission'=>$checkPermission
        ];

        return view('permissions.index',$data);
    }

    public function showViewRole($role_id)
    {
        $permissions = $this->repository->getPermissionMenuCmsByObject('role',$role_id);

        $role = app(RoleRepository::class)->findById($role_id);

        $data = [
            'permissions'=>$permissions,
            'role_id'=>$role_id,
            'role_name'=>$role['display_name']
        ];

        return view('permissions.role', $data);
    }

    public function role(Request $request)
    {
        $permission_id = $request->get('permission_id');
        $role_id = $request->get('role_id');

        /**
         * @var Permission $permission
         */
        $permission = $this->repository->findById($permission_id);
        $roleRepository = app(RoleRepository::class);

        /**
         * @var Role $role
         */
        $role = $roleRepository->findById($role_id);

        $permission_name = $permission->name;

        if (!$role->hasPermissionTo($permission_name)) {
            $role->givePermissionTo($permission);
        } else {
            $role->revokePermissionTo($permission_name);
        }

        return ApiResponse::responseSuccess([
            'data'=>'Thành công'
        ]);
    }

    public function showViewUser($user_id)
    {
        $permissions = $this->repository->getPermissionMenuCmsByObject('user',$user_id);

        $user = app(UserRepository::class)->findById($user_id);

        $data = [
            'permissions' => $permissions,
            'user_id' => $user_id,
            'user_name' => $user['name'],
        ];

        return view('permissions.user',$data);
    }

    public function user(Request $request)
    {
        $permission_id = $request->get('permission_id');
        $user_id = $request->get('user_id');

        /**
         * @var Permission $permission
         */
        $permission = $this->repository->findById($permission_id);
        $userRepository = app(UserRepository::class);

        /**
         * @var User $user
         */
        $user = $userRepository->findById($user_id);

        $permission_name = $permission->name;

        if (!$user->hasPermissionTo($permission_name)) {
            $user->givePermissionTo($permission);
        } else {
            $user->revokePermissionTo($permission_name);
        }

        return ApiResponse::responseSuccess([
            'data'=>'Thành công'
        ]);
    }

    public function edit($permission_id)
    {
        $roles = app(RoleRepository::class)->list();

        $permissionNames = $this->repository->getPermissionChildren($permission_id);

        foreach ($roles as $key => $role) {
            foreach ($permissionNames as $permissionName) {
                $roles[$key][$permissionName['name']] = array_search($permissionName['name'],$role['permission_names'])!==false;
            }
        }

        $menus = $this->repository->getMenuCms();

        $data = [
            'roles'=>$roles,
            'permissionNames'=>$permissionNames,
            'permission'=>$this->repository->findById($permission_id),
            'menus'=>$menus
        ];

        return view("permissions.edit",$data);

    }

    public function create()
    {
        $menus = $this->repository->getMenuCms();

        return view('permissions.create',compact("menus"));
    }

    public function store(Request $request)
    {
        $parent_id = $request->get('parent_id');
        $name = $request->get('name');
        $url = $request->get('url');
        $icon = $request->get('icon');
        $is_display = $request->get('is_display')=="on";
        $description = $request->get('description');

        $data = [
            'parent_id' => $parent_id,
            'name'=>'console_'.Str::slug($url),
            'display_name'=>$name,
            'url'=>$url,
            'icon'=>$icon,
            'is_display'=>$is_display,
            'description'=>$description,
            'guard_name'=>'web',
        ];

        $permission = $this->repository->create($data);

        $consoles = [
            'edit'=>"Sửa",
            'add'=>"Thêm",
            'delete'=>"Xóa",
            'view'=>"Xem"
        ];

        foreach ($consoles as $key=>$console) {
            $this->repository->create([
                'name'=> $data['name']."_".$key,
                'display_name'=>$console,
                'guard_name'=>'web',
                'parent_id'=>$permission->getKey(),
                'model'=>'App\Models\\'.ucwords(Str::slug($url))
            ]);
        }

        $permission_id = $request->get('permission');
        if (!$permission_id) {
            return redirect()->route('permission.index');
        }
        else {
            return [
                'permission_id'=> $permission->getKey()
            ];
        }


    }

    public function update(Request $request,$permission_id)
    {
        $parent_id = $request->get('parent_id');
        $name = $request->get('name');
        $url = $request->get('url');
        $icon = $request->get('icon');
        $is_display = $request->get('is_display')=="on";
        $description = $request->get('description');

        $data = [
            'parent_id' => $parent_id,
            'name'=>'console_'.Str::slug($url),
            'display_name'=>$name,
            'url'=>$url,
            'icon'=>$icon,
            'is_display'=>$is_display,
            'description'=>$description,
            'guard_name'=>'web',
        ];

        /**
         * @var Permission $permission
        */
        $permission = $this->repository->updateOrCreate(
            [
                'id'=>$permission_id
            ],
            $data
        );

        foreach ($permission['children_model'] as $child) {
            $this->repository->updateOrCreate(
                [
                    'id'=>$child['id']
                ],
                [
                    'name'=>$data['name']."_".Str::afterLast($child['name'],"_")
                ]
            );
        }

        return redirect()->route('permission.index');

    }

    public function destroy($permission_id)
    {
        /**
         * @var Permission $permission
        */
        $permission = $this->repository->findById($permission_id);

        $permission->childrenModel()->forceDelete();

        $permission->childrenTree()->update(['parent_id'=>$permission->parent_id]);

        $permission->forceDelete();

        return redirect()->route('permission.index');

    }

}
