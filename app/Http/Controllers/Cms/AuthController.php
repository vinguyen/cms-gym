<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller {


    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        $remember = $request->get('remember');

        $userRepository = app(UserRepository::class);

        $user = $userRepository->findUserByEmailPassword($email, $password);

        if ($user) {
            Auth::login($user);
            return redirect()->route("dashboard");
        }
        else {
            return redirect()->route("showLoginForm");
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

}
