<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Repositories\PackageStudentRepository;

class RevenueController extends Controller
{

    /** @var PackageStudentRepository $repository */
    protected $repository;

    public function __construct()
    {
        $this->repository = app(PackageStudentRepository::class);
    }

    public function index()
    {

        $revenues = $this->repository->getRevenue();

        $total = 0;
        foreach ($revenues as $revenue) {
            $total+=$revenue['package']['price'];
        }

        $data = [
            'revenues'=>$revenues,
            'total'=>$total
        ];

        return view('revenue.index',$data);

    }

}
