<?php

namespace App\Http\Controllers\Cms;

use App\Helpers\UserPermission;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Models\Role;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;

class RoleController extends Controller
{

    /** @var RoleRepository $repository */
    protected $repository;

    /**
     * Permission constructor.
     */
    public function __construct()
    {
        $this->repository = app(RoleRepository::class);
    }

    public function index()
    {
        $roles = $this->repository->list();

        $checkPermission = app(PermissionRepository::class)->getPermissionByRouteModel();

        $data = [
            'roles'=>$roles,
            'checkPermission'=>$checkPermission
        ];

        return view('roles.index',$data);
    }

    public function create()
    {
        return view('roles.create');
    }

    public function store(RoleRequest $request)
    {
        $name = $request->get('name');
        $description = $request->get('description');
        $guard_name = $request->get('guard_name');

        $roleRepository = app(RoleRepository::class);

        $data = [
            'name' => Str::slug($name),
            'display_name'=>$name,
            'description'=>$description,
            'guard_name' => "web"
        ];

        $roleRepository->create($data);

        return redirect()->route('role.index');
    }

    public function edit($role_id)
    {
        $role = $this->repository->findById($role_id)->toArray();

        $data = [
            'role'=>$role
        ];

        return view('roles.edit',$data);

    }

    public function update(RoleRequest $request, $role_id)
    {
        $name = $request->get('name');
        $description = $request->get('description');

        $data = [
            'name' => Str::slug($name),
            'display_name'=>$name,
            'description'=>$description,
        ];

        $this->repository->updateOrCreate(['id'=>$role_id],$data);

        return redirect()->route('role.index');
    }

    public function destroy($role_id)
    {
        try {
            $this->repository->forceDelete($role_id);
        } catch (\Exception $e) {
        }

        return redirect()->route('role.index');
    }

}
