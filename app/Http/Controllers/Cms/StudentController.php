<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Repositories\PackageRepository;
use App\Repositories\PackageStudentRepository;
use App\Repositories\PermissionRepository;
use App\Repositories\StudentRepository;
use Illuminate\Http\Request;

class StudentController extends Controller
{

    /** @var StudentRepository $repository */
    protected $repository;

    /**
     * Permission constructor.
     */
    public function __construct()
    {
        $this->repository = app(StudentRepository::class);
    }

    public function index()
    {
        $students = $this->repository->list();
        $checkPermission = app(PermissionRepository::class)->getPermissionByRouteModel();

        $data = [
            'students'=>$students,
            'checkPermission'=>$checkPermission
        ];

        return view('student.index',$data);
    }

    public function create()
    {

        $packages = app(PackageRepository::class)->list();

        $data = [
            'packages'=>$packages
        ];

        return view('student.create',$data);

    }

    public function store(Request $request)
    {

        $data = [
            'name'=>$request->get('name'),
            'mobile'=>$request->get('mobile'),
            'yearOfBirth'=>$request->get('yearOfBirth'),
            'gender'=>$request->get('gender')
        ];

        /**
         * @var Student $student
        */
        $student = $this->repository->create($data);

        $package_id = $request->get('package_id');

        $packageStudentRepository = app(PackageStudentRepository::class);

        $packageStudentRepository->create([
            'package_id'=>$package_id,
            'student_id'=>$student->getKey()
        ]);

        return redirect()->route('student.index');

    }

    public function edit($student_id)
    {
        $student = $this->repository->findById($student_id)->toArray();

        $data = [
            'student'=>$student
        ];

        return view('student.edit',$data);
    }

    public function update(Request $request, $student_id)
    {
        $data = [
            'name'=>$request->get('name'),
            'mobile'=>$request->get('mobile'),
            'yearOfBirth'=>$request->get('yearOfBirth'),
            'gender'=>$request->get('gender')
        ];

        $this->repository->updateOrCreate(['id'=>$student_id],$data);
        return redirect()->route('student.index');
    }

    public function destroy($student_id)
    {

        try {
            $this->repository->forceDelete($student_id);
        } catch (\Exception $e) {
        }

        return redirect()->route('student.index');

    }

}
