<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\CmsMenuRequest;
use App\Models\CmsMenu;
use App\Repositories\CmsMenuRepository;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;

class CmsMenuController extends Controller
{

    /** @var CmsMenuRepository $repository */
    protected $repository;

    /**
     * Permission constructor.
     */
    public function __construct()
    {
        $this->repository = app(CmsMenuRepository::class);
    }

    public function index()
    {
        $cmsMenuRepository = app(CmsMenuRepository::class);

        $menus = $cmsMenuRepository->list();

        $data = [
            'menus'=>$menus
        ];

        return view('cms-menu.index',$data);
    }

    public function create()
    {
        $cmsMenuRepository = app(CmsMenuRepository::class);
        $menu_parent = $cmsMenuRepository->getMenuParent();

        $data = [
            'menu_parent'=>$menu_parent
        ];

        return view('cms-menu.create',$data);
    }

    public function store(CmsMenuRequest $request)
    {
        $name = $request->get('name');
        $url = $request->get('url');
        $icon = $request->get('icon');
        $description = $request->get('description');
        $parent_id = $request->get('parent_id');

        $data = [
            'name' => $name,
            'url'=>$url,
            'icon'=>$icon,
            'description'=>$description,
            'parent_id'=>$parent_id,
            'permission_code'=>"console_".substr($url,1)
        ];

        $cmsMenuRepository = app(CmsMenuRepository::class);

        $cmsMenuRepository->create($data);

        $permissionRepository = app(PermissionRepository::class);

        $permission = $permissionRepository->create([
            'name'=>$data['permission_code'],
            'display_name'=>$name,
            'guard_name'=>'web'
        ]);
        $consoles = [
            'edit'=>"Sửa",
            'add'=>"Thêm",
            'delete'=>"Xóa",
            'view'=>"Xem"
        ];
        foreach ($consoles as $key=>$console) {
            $permissionRepository->create([
               'name'=> $data['permission_code']."_".$key,
                'display_name'=>$console,
                'guard_name'=>'web',
                'parent_id'=>$permission->id
            ]);
        }

        return redirect()->route('menu_cms.index');

    }

    public function edit($menu_id)
    {
        $menu = $this->repository->findById($menu_id)->toArray();

        $menu_parent = $this->repository->getMenuParent();

        $data = [
            'menu' => $menu,
            'menu_parent'=>$menu_parent
        ];

        return view('cms-menu.edit',$data);

    }

    public function update(CmsMenuRequest $request,$menu_id)
    {
        $url = $request->get('url');

        $data = [
            'name' => $request->get('name'),
            'url'=>$url,
            'icon'=>$request->get('icon'),
            'description'=>$request->get('description'),
            'parent_id'=>$request->get('parent_id'),
            'permission_code'=>"console_".substr($url,1)
        ];


        $this->repository->updateOrCreate(['id' => $menu_id], $data);

        return redirect()->route('menu_cms.index');
    }

    public function destroy($user_id)
    {

        try {
            $this->repository->forceDelete($user_id);
        } catch (\Exception $e) {
        }

        return redirect()->route('menu_cms.index');

    }

}
