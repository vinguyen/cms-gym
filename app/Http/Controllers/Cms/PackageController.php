<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Repositories\PackageRepository;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;

class PackageController extends Controller
{

    /** @var PackageRepository $repository */
    protected $repository;

    /**
     * Permission constructor.
     */
    public function __construct()
    {
        $this->repository = app(PackageRepository::class);
    }

    public function index()
    {
        $packages = $this->repository->list();
        $checkPermission = app(PermissionRepository::class)->getPermissionByRouteModel();

        $data = [
            'packages'=>$packages,
            'checkPermission'=>$checkPermission
        ];

        return view('package.index',$data);
    }

    public function create()
    {
        return view('package.create');
    }

    public function store(Request $request)
    {
        $data = [
            'name' => $request->get('name'),
            'description'=>$request->get('description'),
            'price'=>$request->get('price'),
            'period'=>$request->get('period')
        ];

        $this->repository->create($data);

        return redirect()->route('package.index');
    }

    public function edit($package_id)
    {
        $package = $this->repository->findById($package_id)->toArray();

        $data = [
            'package'=>$package
        ];

        return view('package.edit',$data);
    }

    public function update(Request $request, $package_id)
    {
        $data = [
            'name' => $request->get('name'),
            'description'=>$request->get('description'),
            'price'=>$request->get('price'),
            'period'=>$request->get('period')
        ];

        $this->repository->updateOrCreate(['id'=>$package_id],$data);

        return redirect()->route('package.index');
    }

    public function destroy($package_id)
    {

        try {
            $this->repository->forceDelete($package_id);
        } catch (\Exception $e) {
        }

        return redirect()->route('device.index');

    }


}
