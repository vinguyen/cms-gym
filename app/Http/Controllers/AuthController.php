<?php

namespace App\Http\Controllers;

use App\Helpers\ApiResponse;
use App\Repositories\UserRepository;
use Carbon\Carbon as Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;

class AuthController extends Controller
{

    /**
     * @return JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return ApiResponse::responseError([
                'message'=>'Unauthorized'
            ],201);
        }

        $data = [
            'user' => Auth::user(),
            'access_token' => JWTAuth::fromUser(Auth::user()),
        ];

        return ApiResponse::responseSuccess([
            'data' => $data
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {

        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'password' => md5($request->get('password')),
            'status'=> 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        $userRepository = app(UserRepository::class);

        $user = $userRepository->create($data);

        $token = JWTAuth::fromUser($user);

        $data = [
            'user' => $user,
            'access_token' => $token,
        ];

        return ApiResponse::responseSuccess([
            'data' => $data
        ]);

    }

    /**
     * @return JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return ApiResponse::responseSuccess([
            'message' => 'Successfully logged out'
        ]);
    }

}
