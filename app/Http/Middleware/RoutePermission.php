<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Repositories\CmsMenuRepository;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class RoutePermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        return $next($request);
        $routeName = Route::current()->getName();
        $consoles = explode(".", $routeName);

        $permissionName = "console_".$consoles[0];
        if (count($consoles) > 1) {
            if(array_search($consoles[1],["edit","update","role","user","showViewRole","showViewUser"])!==false) {
                $cons = "edit";
            }
            else if(array_search($consoles[1],["create","store"])!==false) {
                $cons = "add";
            }
            else if(array_search($consoles[1],["delete","destroy"])) {
                $cons = "delete";
            }
            if (!empty($cons)) {
                $permissionName = $permissionName."_".$cons;
            }
        }

        /**
         * @var User $user
         */
        $user = auth()->user();

        $userPermission = $user->getAllPermissions()->pluck('name')->toArray();

        if (in_array($permissionName, $userPermission)) {
            return $next($request);
        }
        else {
            return redirect()->route('unauthorized');
        }

    }
}
