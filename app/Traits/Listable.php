<?php

namespace App\Traits;

use App\Repositories\BaseRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Trait Listable
 * @package App\Traits
 * @property BaseRepository $repository
 */
trait Listable
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request)
    {
        return $this->repository->fetch($request);
    }

}
