<?php

namespace App\Traits;

trait HasStatus
{

    /**
     * @return bool
     */
    public function getDeletedAttribute()
    {
        return $this->trashed();
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        return $this->trashed() ? __('message.inactive') : __('message.active');
    }

    /**
     * @return string
     */
    public function getStatusAttribute()
    {
        return $this->trashed() ? 'disabled' : 'active';
    }
}
