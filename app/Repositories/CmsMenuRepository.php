<?php

namespace App\Repositories;

use App\Models\CmsMenu;
use App\Models\User;

class CmsMenuRepository extends BaseRepository
{

    /**
     * CmsMenuRepository constructor.
     * @param CmsMenu $model
     */
    public function __construct(CmsMenu $model)
    {
        $this->model = $model;
    }

    public function getMenuParent()
    {

        return $this->query()
            ->whereNull('parent_id')
            ->get()
            ->toArray();

    }

    public function list()
    {
        return $this->query()->with('children')
            ->whereNull('parent_id')
            ->get()
            ->toArray();
    }

    public function getMenuUser()
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        $userPermission = $user->getAllPermissions()->pluck('name')->toArray();

        return $this->query()->with(array('children'=> function ($query) use ($userPermission) {
            $query->whereIn('permission_code',$userPermission);
        }))
            ->whereNull('parent_id')
            ->get()
            ->toArray();
    }

}
