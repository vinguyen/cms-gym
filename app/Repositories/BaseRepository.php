<?php

namespace App\Repositories;

use App\Helpers\ApiResponse;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Throwable;
use Exception;


abstract class BaseRepository
{

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var null|Model
     */
    protected $instanceWithTrash = null;

    /**
     * @var null|Model
     */
    protected $instanceWithoutTrash = null;

    /**
     * @return Model
     */
    protected function getModel()
    {
        return $this->model;
    }

    /**
     * @return Builder|SoftDeletes
     */
    protected function query()
    {
        return $this->getModel()->newQuery();
    }

    /**
     * @return string
     */
    public function getTable()
    {
        if (! $this->table) {
            $this->table = $this->getModel()->getTable();
        }

        return $this->table;
    }

    /**
     * @param $attributes
     * @return Model
     */
    public function create($attributes)
    {
        return $this->query()->create($attributes);
    }

    /**
     * @param array $attributes
     * @param array $value
     * @return Builder|Model
     */
    public function updateOrCreate(array $attributes, array $value)
    {
        return $this->query()->withTrashed()->updateOrCreate($attributes, $value);
    }

    /**
     * @param $attributes
     * @param $value
     * @return Builder|Model
     */
    public function firstOrCreate($attributes, $value = [])
    {
        return $this->query()->withTrashed()->firstOrCreate($attributes, $value);
    }

    /**
     * @return string
     */
    public function getKeyName()
    {
        return $this->getModel()->getKeyName();
    }

    /**
     * @param $id
     * @return Builder|Model
     */
    public function findById($id)
    {
        if (! $this->instanceWithTrash) {
            $this->instanceWithTrash = $this->query()
                ->withTrashed()
                ->where($this->getKeyName(), $id)
                ->firstOrFail();
        }

        return $this->instanceWithTrash;
    }

    /**
     * @param $id
     * @return bool|void
     * @throws Exception
     */
    public function delete($id)
    {
        /** @var Model $model */
        $model = $this->findById($id);

        $model->delete();
    }

    /**
     * @param $id
     * @return bool|void
     * @throws Exception
     */
    public function forceDelete($id)
    {
        /** @var Model $model */
        $model = $this->findById($id);

        $model->forceDelete();
    }

    /**
     * @param $id
     */
    public function restore($id)
    {
        /** @var SoftDeletes $model */
        $model = $this->findById($id);

        try {
            $model->restore();
        } catch (Throwable $exception) {

        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function fetch(Request $request)
    {
        $query = $this->query()->withTrashed();

        /**
         * load relations
         */
        if (method_exists($this, 'loadRelations')) {
            $this->loadRelations($query);
        }

        /** apply filter */
        if (method_exists($this, 'applyFilter')) {
            $this->applyFilter($query, $request);
        }

        /** fetch */
        $items = $query->get();

        /**
         * prepare data
         */
        if (method_exists($this, 'prepareDataToResponse')) {
            $this->prepareDataToResponse($items, $request);
        }

        $data = [
            'items' => $items->toArray(),
        ];

        return ApiResponse::responseSuccess([
            'data' => $data,
        ]);

    }

    public function getRouteModelCurrent()
    {
        $routeName = Route::current()->getName();
        return explode(".", $routeName)[0];
    }

    public function list()
    {
        return $this->query()->get()->toArray();
    }

}
