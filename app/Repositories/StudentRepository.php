<?php

namespace App\Repositories;

use App\Models\Student;

class StudentRepository extends BaseRepository
{

    /**
     * StudentRepository constructor.
     * @param Student $model
     */
    public function __construct(Student $model)
    {
        $this->model = $model;
    }

}
