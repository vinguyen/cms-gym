<?php

namespace App\Repositories;

use App\Helpers\UserPermission;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Str;

class PermissionRepository extends BaseRepository
{
    /**
     * PermissionRepository constructor.
     * @param Permission $model
     */
    public function __construct(Permission $model)
    {
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function fetchPermissions()
    {
        return $this->query()
            ->whereNull('parent_id')
            ->with('children')
            ->get()
            ->toArray();
    }

    public function getAllPermissionsByObject($object_type, $object_id)
    {
        if ($object_type == 'role') {
            /**
             * @var Role $object
            */
            $object = app(RoleRepository::class)->findById($object_id);
        }
        else {
            /**
             * @var User $object
            */
            $object = app(UserRepository::class)->findById($object_id);
        }
        $objectPermission = $object->getAllPermissions()->pluck('name')->toArray();
        $permissions = $this->fetchPermissions();
        foreach ($permissions as $key => $permission) {
            $permissions[$key]['checked'] = in_array($permission['name'],$objectPermission);
            if ($permissions[$key]['children']) {
                foreach ($permissions[$key]['children'] as $ke => $child) {
                    $permissions[$key]['children'][$ke]['checked'] = in_array($child['name'],$objectPermission);
                }
            }
        }
        return $permissions;
    }

    public function getPermissionChecked($permissions,$objectPermission)
    {
        foreach ($permissions as $key => $permission) {
            $permissions[$key]['checked'] = in_array($permission['name'],$objectPermission);
            if ($permissions[$key]['children_model']) {
                foreach ($permissions[$key]['children_model'] as $ke => $child) {
                    $permissions[$key]['children_model'][$ke]['checked'] = in_array($child['name'],$objectPermission);
                }
            }
            if (count($permissions[$key]['children'])) {
                $permissions[$key]['children'] = $this->getPermissionChecked($permissions[$key]['children'],$objectPermission);
            }
        }
        return $permissions;
    }

    public function getPermissionChildren($permission_id)
    {

        $permission = $this->query()
            ->where('id',$permission_id)
            ->first()->toArray();

        $permissionChild = [];

        array_push($permissionChild,[
            'id'=>$permission['id'],
            'name'=>$permission['name']
        ]);

        sort($permission['children_model']);

        foreach ($permission['children_model'] as $child) {
            array_push($permissionChild,[
                'id'=>$child['id'],
                'name'=>$child['name']
            ]);
        }

        return $permissionChild;
    }

    public function getAllPermission()
    {
        return $this->query()
            ->whereNull('parent_id')
            ->get()
            ->toArray();
    }

    public function getMenuCms()
    {
        return $this->query()
            ->whereNull('parent_id')
            ->where('is_display',1)
            ->get()
            ->toArray();
    }

    public function getPermissionMenuCmsByObject($object_type, $object_id)
    {
        if ($object_type == 'role') {
            /**
             * @var Role $object
             */
            $object = app(RoleRepository::class)->findById($object_id);
        }
        else {
            /**
             * @var User $object
             */
            $object = app(UserRepository::class)->findById($object_id);
        }
        $objectPermission = $object->getAllPermissions()->pluck('name')->toArray();

        $permissions = $this->getMenuCms();

        return $this->getPermissionChecked($permissions, $objectPermission);

    }

    public function getMenuPermissionUser()
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        $userPermission = $user->getAllPermissions()->pluck('name')->toArray();

        return $this->query()
            ->whereNull('parent_id')
            ->where('is_display',1)
            ->whereIn('name',$userPermission)
            ->get()
            ->toArray();

    }

    public function getPermissionByRouteModel()
    {

        $route = $this->getRouteModelCurrent();

        $userPermission = UserPermission::getAllPermissionByUser();

        $permissionModel = $this->query()
            ->where('name','console_'.$route)
            ->first();

        $result['browser'] = in_array($permissionModel['name'],$userPermission);

        foreach ($permissionModel['children_model'] as $child) {
            $result[Str::afterLast($child['name'],'_')] = in_array($child['name'],$userPermission);
        }

        return $result;
    }
}
