<?php

namespace App\Repositories;

use App\Models\PackageStudent;

class PackageStudentRepository extends BaseRepository
{

    /**
     * PackageStudentRepository constructor.
     * @param PackageStudent $model
     */
    public function __construct(PackageStudent $model)
    {
        $this->model = $model;
    }

    public function getRevenue()
    {

        return $this->query()->with(['package', 'student'])
            ->get()
            ->toArray();
    }
}
