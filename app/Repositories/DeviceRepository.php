<?php

namespace App\Repositories;

use App\Models\Device;

class DeviceRepository extends BaseRepository
{

    /**
     * DeviceRepository constructor.
     * @param Device $model
     */
    public function __construct(Device $model)
    {
        $this->model = $model;
    }

}
