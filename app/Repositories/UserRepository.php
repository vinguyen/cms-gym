<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{

    /**
     * UserRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function findUserByEmailPassword($email, $password)
    {

        return $this->query()
            ->where('email', $email)
            ->where('password', md5($password))
            ->first();
    }

    public function list()
    {
        return $this->query()->get()->toArray();
    }

}
