<?php

namespace App\Repositories;

use App\Models\Package;

class PackageRepository extends BaseRepository
{

    /**
     * PackageRepository constructor.
     * @param Package $model
     */
    public function __construct(Package $model)
    {
        $this->model = $model;
    }

}
