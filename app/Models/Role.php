<?php

namespace App\Models;

use App\Repositories\RoleRepository;
use App\Traits\HasStatus;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Models\Role as BaseRole;

class Role extends BaseRole
{
    use SoftDeletes;
    use HasStatus;

    /**
     * @inheritdoc
     * @var array
     */
    protected $hidden = [
        'guard_name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @inheritdoc
     * @var array
     */
    protected $appends = [
        'status',
        'status_label',
        'deleted',
        'permission_names'
    ];

    /**
     * @return RoleRepository
     */
    public function getRepositoryInstance()
    {
        return app(RoleRepository::class);
    }

    public function getPermissionNamesAttribute()
    {
        return $this->getAllPermissions()->pluck('name')->toArray();
    }

}
