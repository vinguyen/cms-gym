<?php

namespace App\Models;

use App\Repositories\DeviceRepository;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Device
 * @package App\Models
 * @property int $id
 * @property int $created_at
 * @property int $amount
 * @property int $description
 */


class Device extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * @inheritdoc
     * @var string
     */
    protected $table = 'devices';

    /**
     * @inheritdoc
     * @var int
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    protected $appends = [
        'created_time'
    ];

    /**
     * @return DeviceRepository
     */
    public function getRepositoryInstance()
    {
        return app(DeviceRepository::class);
    }

    public function getCreatedTimeAttribute()
    {
        return date_format($this->created_at,"H:i:s d/m/Y");
    }


}
