<?php

namespace App\Models;

use App\Repositories\UserRepository;
use App\Traits\HasStatus;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 * @package App\Models
 * @property int $user_id
 * @property string $name
 * @property string $password
 * @property string $phone
 * @property string $phone_verified_at
 * @property string $email
 * @property string $email_verified_at
 * @property string $remember_token
 * @property string $avatar
 * @property string $role_name
 * @property Carbon $created_at
 */

class User extends Authenticatable implements JWTSubject
{
    use HasStatus,SoftDeletes;
    use Notifiable;
    use HasRoles;

    /**
     * @inheritdoc
     * @var string
     */
    protected $table = 'users';

    /**
     * @inheritdoc
     * @var int
     */
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'user_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
        'created_at',
        'deleted_at',
        'password'
    ];

    protected $appends = [
        'role_name',
        'role_display_name'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @return UserRepository
     */
    public function getRepositoryInstance()
    {
        return app(UserRepository::class);
    }

    public function getRoleNameAttribute()
    {
        return $this->roles()->pluck('name')->first();
    }

    public function getRoleDisplayNameAttribute()
    {
        return $this->roles()->pluck('display_name')->first();
    }

}
