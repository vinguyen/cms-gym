<?php

namespace App\Models;

use App\Repositories\StudentRepository;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * @inheritdoc
     * @var string
     */
    protected $table = 'students';

    /**
     * @inheritdoc
     * @var int
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    protected $appends = [
        'package_name'
    ];

    /**
     * @return StudentRepository
     */
    public function getRepositoryInstance()
    {
        return app(StudentRepository::class);
    }

    public function packages()
    {
        return $this->hasMany(PackageStudent::class,'student_id','id');
    }

    public function getPackageNameAttribute()
    {
        return $this->packages()->first()->package->name;
    }

}
