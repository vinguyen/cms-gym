<?php

namespace App\Models;

use App\Repositories\CmsMenuRepository;
use App\Traits\HasStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Route;

/**
 * Class CmsMenu
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $permission_code
 * @property boolean $display
 * @property int $order
 * @property int $parent_id
 * @property boolean $is_parent
 * @property string $icon
 */
class CmsMenu extends Model
{

    use HasStatus,SoftDeletes;

    /**
     * @inheritdoc
     * @var string
     */
    protected $table = 'cms_menus';

    /**
     * @inheritdoc
     * @var int
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @inheritdoc
     * @var array
     */
    protected $hidden = [
        'updated_at',
        'created_at',
        'deleted_at'
    ];

    /**
     * @inheritdoc
     * @var array
     */
    protected $appends = [
        'is_parent',
        'is_active'
    ];

    /**
     * @return CmsMenuRepository
     */
    public function getRepositoryInstance()
    {
        return app(CmsMenuRepository::class);
    }

    public function getIsParentAttribute()
    {
        return empty($this->parent_id);
    }

    public function children()
    {
        return $this->hasMany(CmsMenu::class,'parent_id','id');
    }

    public function getIsActiveAttribute()
    {
        $routeName = Route::current()->getName();
        $permission = substr($routeName,0,strpos($routeName,"."));
        $menu_active = "console_".$permission;
        if ($this->parent_id) {
            return $menu_active == $this->permission_code;
        }
        else {
            $permissionCodes = $this->children()->pluck('permission_code')->toArray();
            return in_array($menu_active,$permissionCodes);
        }

    }

}
