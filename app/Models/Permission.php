<?php

namespace App\Models;
use App\Repositories\PermissionRepository;
use App\Traits\HasStatus;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission as BasePermission;

/**
 * Class Permission
 * @package App\Models
 * @property string $name
 * @property string $display_name
 * @property integer $parent_id
 */

class Permission extends BasePermission
{
    use HasStatus, SoftDeletes;

    /**
     * @inheritdoc
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'guard_name',
        'parent_id'
    ];

    /**
     * @inheritdoc
     * @var array
     */
    protected $appends = [
        'status_label',
        'deleted',
        'status',
        'children',
        'children_model',
        'menu_children',
        'is_active',
        'menu_children_user'
    ];

    /**
     * @return PermissionRepository
     */
    public function getRepositoryInstance()
    {
        return app(PermissionRepository::class);
    }

    public function children()
    {
        return $this->hasMany(Permission::class, 'parent_id', 'id')->orderBy('name','desc');
    }

    public function childrenModel()
    {
        return $this->hasMany(Permission::class,'parent_id','id')
            ->whereNotNull('model')
            ->orderBy('name','desc');
    }

    public function childrenTree()
    {
        return $this->hasMany(Permission::class,'parent_id','id')
            ->whereNull('model')
            ->orderBy('name','desc');
    }

    public function getChildrenAttribute()
    {
        return $this->children()
            ->whereNull('model')
            ->get()
            ->toArray();
    }

    public function getChildrenModelAttribute()
    {
        return $this->children()
            ->whereNotNull('model')
            ->orderBy('name','desc')
            ->get()
            ->toArray();
    }

    public function getMenuChildrenAttribute()
    {
        return $this->children()
            ->whereNull('model')
            ->where('is_display',1)
            ->get()
            ->toArray();
    }

    public function getIsActiveAttribute()
    {
        $routeName = Route::current()->getName();
        $permission = substr($routeName,0,strpos($routeName,"."));
        $menu_active = "console_".$permission;
        if ($this->parent_id) {
            return $menu_active == $this->name;
        }
        else {
            $permissionChild = $this->children()->pluck('name')->toArray();
            return in_array($menu_active,$permissionChild);
        }
    }

    public function getMenuChildrenUserAttribute()
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        $userPermission = $user->getAllPermissions()->pluck('name')->toArray();

        return $this->children()
            ->whereNull('model')
            ->where('is_display',1)
            ->whereIn('name',$userPermission)
            ->get()
            ->toArray();
    }

}
