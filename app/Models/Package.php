<?php

namespace App\Models;

use App\Repositories\PackageRepository;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * @inheritdoc
     * @var string
     */
    protected $table = 'packages';

    /**
     * @inheritdoc
     * @var int
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * @return PackageRepository
     */
    public function getRepositoryInstance()
    {
        return app(PackageRepository::class);
    }

}
