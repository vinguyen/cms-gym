<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageStudent extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * @inheritdoc
     * @var string
     */
    protected $table = 'package_students';

    /**
     * @inheritdoc
     * @var int
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    public function package()
    {
        return $this->belongsTo(Package::class,'package_id','id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class,'student_id','id');
    }

}
