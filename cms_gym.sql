/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : cms_gym

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 05/12/2020 22:25:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cms_menus
-- ----------------------------
DROP TABLE IF EXISTS `cms_menus`;
CREATE TABLE `cms_menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display` tinyint(1) NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `permission_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `order` int(11) NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_menus
-- ----------------------------
INSERT INTO `cms_menus` VALUES (1, 'Quản trị', NULL, NULL, 'Quản trị', NULL, NULL, 'fa fa-group', NULL, '2020-11-21 03:56:01', '2020-11-21 03:56:01', NULL);
INSERT INTO `cms_menus` VALUES (2, 'Phân quyền', NULL, 1, NULL, 'console_permission', NULL, NULL, NULL, '2020-11-21 04:15:50', '2020-11-21 04:15:50', '/permission');
INSERT INTO `cms_menus` VALUES (3, 'Quản lý menu cms', NULL, 1, NULL, 'console_menu_cms', NULL, NULL, NULL, '2020-11-21 04:18:14', '2020-11-21 04:18:14', '/menu_cms');
INSERT INTO `cms_menus` VALUES (4, 'Quản trị viên', NULL, 1, NULL, 'console_admin', NULL, NULL, NULL, '2020-11-21 04:43:46', '2020-11-21 04:43:46', '/admin');
INSERT INTO `cms_menus` VALUES (8, 'Vai trò', NULL, 1, NULL, 'console_role', NULL, NULL, NULL, '2020-11-22 15:11:13', '2020-11-22 15:11:13', '/role');
INSERT INTO `cms_menus` VALUES (9, 'Thiết lập', NULL, NULL, 'Thiết lập', NULL, NULL, 'fa fa-gears', NULL, NULL, NULL, NULL);
INSERT INTO `cms_menus` VALUES (12, 'Phiên bản', NULL, 9, NULL, 'console_version', NULL, NULL, NULL, '2020-11-24 16:28:46', '2020-11-24 16:28:46', '/version');
INSERT INTO `cms_menus` VALUES (13, 'Tổng quan hệ thống', NULL, 9, NULL, 'console_', NULL, NULL, NULL, '2020-11-25 04:51:31', '2020-11-25 05:12:19', NULL);
INSERT INTO `cms_menus` VALUES (18, 'Quản lý IKH', NULL, 13, NULL, 'console_', NULL, NULL, NULL, '2020-11-25 05:01:04', '2020-11-25 05:01:04', NULL);
INSERT INTO `cms_menus` VALUES (19, 'Quản lý ABC', NULL, 13, NULL, 'console_', NULL, NULL, NULL, '2020-11-25 05:11:28', '2020-11-25 05:11:40', NULL);

-- ----------------------------
-- Table structure for devices
-- ----------------------------
DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `amount` int(11) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of devices
-- ----------------------------
INSERT INTO `devices` VALUES (1, 'Tạ tay 5KG', 'Thiết bị tập tay', 10, NULL, '2020-12-05 13:13:44', '2020-12-05 13:24:28');
INSERT INTO `devices` VALUES (2, 'Máy tập đá đùi Elip', NULL, 2, NULL, '2020-12-05 13:19:38', '2020-12-05 13:19:38');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (3, '2020_11_19_174354_create_permission_tables', 1);
INSERT INTO `migrations` VALUES (4, '2020_11_20_140431_create_cms_menus_table', 1);
INSERT INTO `migrations` VALUES (5, '2020_11_21_041720_add_url_to_table_cms_menus', 2);
INSERT INTO `migrations` VALUES (6, '2020_11_22_153322_add_soft_delete_and_status_to_role_table', 3);
INSERT INTO `migrations` VALUES (7, '2020_11_22_155049_add_display_name_to_roles_table', 4);
INSERT INTO `migrations` VALUES (8, '2020_11_24_124756_add_parent_id_to_permissions_table', 5);
INSERT INTO `migrations` VALUES (9, '2020_11_25_140418_add_column_model_to_permissions_table', 6);
INSERT INTO `migrations` VALUES (10, '2020_11_25_142724_add_column_is_display_to_permissions_table', 7);
INSERT INTO `migrations` VALUES (11, '2020_12_05_115647_create_devices_table', 8);
INSERT INTO `migrations` VALUES (12, '2020_12_05_115702_create_packages_table', 8);
INSERT INTO `migrations` VALUES (13, '2020_12_05_115708_create_students_table', 8);
INSERT INTO `migrations` VALUES (14, '2020_12_05_135632_add_soft_delete_to_packages_table', 9);

-- ----------------------------
-- Table structure for package_students
-- ----------------------------
DROP TABLE IF EXISTS `package_students`;
CREATE TABLE `package_students`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NULL DEFAULT NULL,
  `student_id` int(11) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of package_students
-- ----------------------------
INSERT INTO `package_students` VALUES (2, NULL, 2, NULL, '2020-12-05 14:42:59', '2020-12-05 14:42:59');
INSERT INTO `package_students` VALUES (3, 2, 3, NULL, '2020-12-05 14:44:17', '2020-12-05 14:44:17');
INSERT INTO `package_students` VALUES (4, 3, 4, NULL, '2020-12-05 15:23:38', '2020-12-05 15:23:38');

-- ----------------------------
-- Table structure for packages
-- ----------------------------
DROP TABLE IF EXISTS `packages`;
CREATE TABLE `packages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `period` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of packages
-- ----------------------------
INSERT INTO `packages` VALUES (1, 'Gói ngày', NULL, '100000', '1', '2020-12-05 13:57:29', '2020-12-05 13:57:29', NULL);
INSERT INTO `packages` VALUES (2, 'Gói tuần', NULL, '600000', '7', '2020-12-05 13:57:51', '2020-12-05 13:57:51', NULL);
INSERT INTO `packages` VALUES (3, 'Gói tháng', NULL, '2000000', '30', '2020-12-05 13:58:15', '2020-12-05 13:58:15', NULL);

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_display` tinyint(4) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `order` int(11) NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (2, 'console_role', 'web', NULL, 'Vai trò', 5, NULL, 1, '/role', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (3, 'console_user', 'web', NULL, 'Người dùng', 5, NULL, 1, '/user', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (5, 'console_permission', 'web', NULL, 'Phân quyền', NULL, NULL, 1, '/permission', 'fa fa-group', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (9, 'console_role_view', 'web', NULL, 'Xem', 2, 'App\\Models\\Role', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (10, 'console_role_edit', 'web', NULL, 'Sửa', 2, 'App\\Models\\Role', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (11, 'console_role_delete', 'web', NULL, 'Xóa', 2, 'App\\Models\\Role', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (12, 'console_role_add', 'web', NULL, 'Thêm', 2, 'App\\Models\\Role', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (13, 'console_user_view', 'web', NULL, 'Xem', 3, 'App\\Models\\User', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (14, 'console_user_edit', 'web', NULL, 'Sửa', 3, 'App\\Models\\User', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (15, 'console_user_delete', 'web', NULL, 'Xóa', 3, 'App\\Models\\User', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (16, 'console_user_add', 'web', NULL, 'Thêm', 3, 'App\\Models\\User', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (21, 'console_permission_view', 'web', NULL, 'Xem', 5, 'App\\Models\\Permission', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (22, 'console_permission_edit', 'web', NULL, 'Sửa', 5, 'App\\Models\\Permission', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (23, 'console_permission_delete', 'web', NULL, 'Xóa', 5, 'App\\Models\\Permission', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (24, 'console_permission_add', 'web', NULL, 'Thêm', 5, 'App\\Models\\Permission', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (28, 'console_permission', 'web', NULL, 'Trang quản trị', 5, NULL, 1, '/permission', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (29, 'console_permission_add', 'web', NULL, 'Thêm', 28, 'App\\Models\\Permission', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (30, 'console_permission_edit', 'web', NULL, 'Sửa', 28, 'App\\Models\\Permission', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (31, 'console_permission_view', 'web', NULL, 'Xem', 28, 'App\\Models\\Permission', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (32, 'console_permission_delete', 'web', NULL, 'Xóa', 28, 'App\\Models\\Permission', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `permissions` VALUES (80, 'console_manage', 'web', NULL, 'Quản lý', NULL, NULL, 1, '/manage', 'fa fa-bar-chart', NULL, NULL, '2020-12-05 11:47:01', '2020-12-05 11:44:46', NULL);
INSERT INTO `permissions` VALUES (81, 'console_manage_edit', 'web', NULL, 'Sửa', 80, 'App\\Models\\', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:47:01', '2020-12-05 11:44:46', NULL);
INSERT INTO `permissions` VALUES (82, 'console_manage_add', 'web', NULL, 'Thêm', 80, 'App\\Models\\', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:47:01', '2020-12-05 11:44:46', NULL);
INSERT INTO `permissions` VALUES (83, 'console_manage_delete', 'web', NULL, 'Xóa', 80, 'App\\Models\\', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:47:01', '2020-12-05 11:44:46', NULL);
INSERT INTO `permissions` VALUES (84, 'console_manage_view', 'web', NULL, 'Xem', 80, 'App\\Models\\', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:47:01', '2020-12-05 11:44:46', NULL);
INSERT INTO `permissions` VALUES (85, 'console_student', 'web', NULL, 'Học viên', 80, NULL, 1, '/student', NULL, NULL, NULL, '2020-12-05 11:48:12', '2020-12-05 11:45:55', NULL);
INSERT INTO `permissions` VALUES (86, 'console_student_edit', 'web', NULL, 'Sửa', 85, 'App\\Models\\Student', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:45:55', '2020-12-05 11:45:55', NULL);
INSERT INTO `permissions` VALUES (87, 'console_student_add', 'web', NULL, 'Thêm', 85, 'App\\Models\\Student', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:45:55', '2020-12-05 11:45:55', NULL);
INSERT INTO `permissions` VALUES (88, 'console_student_delete', 'web', NULL, 'Xóa', 85, 'App\\Models\\Student', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:45:55', '2020-12-05 11:45:55', NULL);
INSERT INTO `permissions` VALUES (89, 'console_student_view', 'web', NULL, 'Xem', 85, 'App\\Models\\Student', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:45:55', '2020-12-05 11:45:55', NULL);
INSERT INTO `permissions` VALUES (95, 'console_device', 'web', NULL, 'Thiết bị', 80, NULL, 1, '/device', NULL, NULL, NULL, '2020-12-05 11:47:49', '2020-12-05 11:47:41', NULL);
INSERT INTO `permissions` VALUES (96, 'console_device_edit', 'web', NULL, 'Sửa', 95, 'App\\Models\\Device', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:47:41', '2020-12-05 11:47:41', NULL);
INSERT INTO `permissions` VALUES (97, 'console_device_add', 'web', NULL, 'Thêm', 95, 'App\\Models\\Device', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:47:41', '2020-12-05 11:47:41', NULL);
INSERT INTO `permissions` VALUES (98, 'console_device_delete', 'web', NULL, 'Xóa', 95, 'App\\Models\\Device', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:47:41', '2020-12-05 11:47:41', NULL);
INSERT INTO `permissions` VALUES (99, 'console_device_view', 'web', NULL, 'Xem', 95, 'App\\Models\\Device', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:47:41', '2020-12-05 11:47:41', NULL);
INSERT INTO `permissions` VALUES (100, 'console_package', 'web', NULL, 'Gói tập', 80, NULL, 1, '/package', NULL, NULL, NULL, '2020-12-05 11:50:35', '2020-12-05 11:50:19', NULL);
INSERT INTO `permissions` VALUES (101, 'console_package_edit', 'web', NULL, 'Sửa', 100, 'App\\Models\\Package', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:50:19', '2020-12-05 11:50:19', NULL);
INSERT INTO `permissions` VALUES (102, 'console_package_add', 'web', NULL, 'Thêm', 100, 'App\\Models\\Package', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:50:19', '2020-12-05 11:50:19', NULL);
INSERT INTO `permissions` VALUES (103, 'console_package_delete', 'web', NULL, 'Xóa', 100, 'App\\Models\\Package', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:50:19', '2020-12-05 11:50:19', NULL);
INSERT INTO `permissions` VALUES (104, 'console_package_view', 'web', NULL, 'Xem', 100, 'App\\Models\\Package', NULL, NULL, NULL, NULL, NULL, '2020-12-05 11:50:19', '2020-12-05 11:50:19', NULL);

-- ----------------------------
-- Table structure for role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `role_has_permissions_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_has_permissions
-- ----------------------------
INSERT INTO `role_has_permissions` VALUES (2, 1);
INSERT INTO `role_has_permissions` VALUES (2, 2);
INSERT INTO `role_has_permissions` VALUES (3, 1);
INSERT INTO `role_has_permissions` VALUES (3, 2);
INSERT INTO `role_has_permissions` VALUES (5, 1);
INSERT INTO `role_has_permissions` VALUES (5, 2);
INSERT INTO `role_has_permissions` VALUES (9, 1);
INSERT INTO `role_has_permissions` VALUES (9, 2);
INSERT INTO `role_has_permissions` VALUES (10, 1);
INSERT INTO `role_has_permissions` VALUES (10, 2);
INSERT INTO `role_has_permissions` VALUES (11, 1);
INSERT INTO `role_has_permissions` VALUES (12, 1);
INSERT INTO `role_has_permissions` VALUES (13, 1);
INSERT INTO `role_has_permissions` VALUES (13, 2);
INSERT INTO `role_has_permissions` VALUES (14, 1);
INSERT INTO `role_has_permissions` VALUES (14, 2);
INSERT INTO `role_has_permissions` VALUES (15, 1);
INSERT INTO `role_has_permissions` VALUES (16, 1);
INSERT INTO `role_has_permissions` VALUES (21, 1);
INSERT INTO `role_has_permissions` VALUES (21, 2);
INSERT INTO `role_has_permissions` VALUES (22, 1);
INSERT INTO `role_has_permissions` VALUES (22, 2);
INSERT INTO `role_has_permissions` VALUES (23, 1);
INSERT INTO `role_has_permissions` VALUES (24, 1);
INSERT INTO `role_has_permissions` VALUES (80, 1);
INSERT INTO `role_has_permissions` VALUES (81, 1);
INSERT INTO `role_has_permissions` VALUES (82, 1);
INSERT INTO `role_has_permissions` VALUES (83, 1);
INSERT INTO `role_has_permissions` VALUES (84, 1);
INSERT INTO `role_has_permissions` VALUES (85, 1);
INSERT INTO `role_has_permissions` VALUES (86, 1);
INSERT INTO `role_has_permissions` VALUES (87, 1);
INSERT INTO `role_has_permissions` VALUES (88, 1);
INSERT INTO `role_has_permissions` VALUES (89, 1);
INSERT INTO `role_has_permissions` VALUES (95, 1);
INSERT INTO `role_has_permissions` VALUES (96, 1);
INSERT INTO `role_has_permissions` VALUES (97, 1);
INSERT INTO `role_has_permissions` VALUES (98, 1);
INSERT INTO `role_has_permissions` VALUES (99, 1);
INSERT INTO `role_has_permissions` VALUES (100, 1);
INSERT INTO `role_has_permissions` VALUES (101, 1);
INSERT INTO `role_has_permissions` VALUES (102, 1);
INSERT INTO `role_has_permissions` VALUES (103, 1);
INSERT INTO `role_has_permissions` VALUES (104, 1);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'admin', 'web', '2020-11-22 15:59:25', '2020-11-22 15:59:25', NULL, NULL, 'Admin');
INSERT INTO `roles` VALUES (2, 'manager', 'web', '2020-11-22 15:59:39', '2020-11-22 15:59:39', NULL, NULL, 'Manager');

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `yearOfBirth` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students
-- ----------------------------
INSERT INTO `students` VALUES (3, 'Học viên A', '0333111333', '1998', 'Nam', NULL, '2020-12-05 14:44:17', '2020-12-05 15:19:55');
INSERT INTO `students` VALUES (4, 'Học viên B', '0333111311', '1999', 'Nữ', NULL, '2020-12-05 15:23:38', '2020-12-05 15:23:38');

-- ----------------------------
-- Table structure for user_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `user_has_permissions`;
CREATE TABLE `user_has_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `user_id`, `model_type`) USING BTREE,
  INDEX `model_has_permissions_model_id_model_type_index`(`user_id`, `model_type`) USING BTREE,
  CONSTRAINT `user_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_has_roles`;
CREATE TABLE `user_has_roles`  (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `user_id`, `model_type`) USING BTREE,
  INDEX `model_has_roles_model_id_model_type_index`(`user_id`, `model_type`) USING BTREE,
  CONSTRAINT `user_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_has_roles
-- ----------------------------
INSERT INTO `user_has_roles` VALUES (1, 'App\\Models\\User', 1);
INSERT INTO `user_has_roles` VALUES (2, 'App\\Models\\User', 2);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `phone_verified_at` timestamp(0) NULL DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  UNIQUE INDEX `users_phone_unique`(`phone`) USING BTREE,
  INDEX `users_status_index`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Administrator', 'admin@gmail.com', '0988654321', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, 1, NULL, NULL, '2020-11-21 02:17:20', '2020-12-05 14:06:56');
INSERT INTO `users` VALUES (2, 'Nguyễn Văn A', 'manager@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL, 1, NULL, NULL, '2020-11-22 18:42:45', '2020-11-24 16:12:53');

SET FOREIGN_KEY_CHECKS = 1;
